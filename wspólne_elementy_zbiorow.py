# -*- coding: utf-8 -*-
"""
@author: Karolina Majka
"""

def wspolne(x, y):
    
    wspolne_elem = []
    # tworzę listę do przechowywania częsci wspólnej 
    # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 
    # slajd 56
    
    
    for elem in x:
        if elem in y:
            # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf
            # slajd 47
            # użyłam pętli for aby dla każdego elementu w zbiorze x
            # sprawdzić czy znajduje się on również w zbiorze y
            wspolne_elem.append(elem)
            y.remove(elem)  
            # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf
            # slajd 59
            # Jeśli element ze zbioru x znajduje się w zbiorze y 
            #cdodajemy go do listy wspolne_elem i usuwamy ten element z y
            # aby uniknąć powtórzeń.
    
    return wspolne_elem
# Funkcja zwraca listę wspolne_elem, która zawiera część wspólną obu zbiorów.


x = [1, 2, 2, 3, 4]
y = [2, 2, 3, 5]

print(wspolne(x, y))  

# Testujemy naszą funkcję dla wyżej podanego zbioru x i y. 
# W konsoli mają wyskoczyć jedynie elementy wspólne 
