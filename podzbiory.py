# -*- coding: utf-8 -*-
"""
@author: Karolina Majka
"""
def podzbiory(x):
    # Tworzę funkcję podzbiory, która zawiera element x
    if not isinstance(x, list):
        # https://stackoverflow.com/questions/11748671/negative-form-of-isinstance-in-python
        # https://eportal.pwr.edu.pl/pluginfile.php/395220/mod_resource/content/1/TP_wyklad.pdf 8
        # google translator - set jako zbiór po angielsku
        # w tej częsci kodu chce aby program sprawdził czy x jest zbiorem
        raise ValueError("Argument musi być listą")
        # Jesli x nie jest zbiorem to funkcja zwraca wyjątek z komunikatem
        # https://eportal.pwr.edu.pl/pluginfile.php/395220/mod_resource/content/1/TP_wyklad.pdf 8 i 9
    if len(x) == 0:
        # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf
        # funkcje len znalazłam w slajdzie 66
        # == slajd 33
        # ogólnie chodzi o to aby sprawdzić czy elementy zbioru x są, czy zbiór jest pusty
        return [set()]
        # jesli zbiór jest pusty tu program zwraca pusty zbiór
        element = x.pop()
        # usuwa jeden element ze zbioru x i przypisuje go do zmiennej element.
        # https://www.w3schools.com/python/ref_list_pop.asp
    subsets_without_element = podzbiory(x[:])
    #  wywołuje funkcję podzbiory dla zbioru x bez usuniętego wcześniej elementu.
    # Wynik jest przypisywany do zmiennej subsets_without_element.
    # subset - podzbór przetłumaczone za pomocą translatora
    return subsets_without_element + [{element} | subset for subset in subsets_without_element]
#  tworzy listę podzbiorów, dodając do każdego podzbioru z listy subsets_without_element usunięty wcześniej element.
#  Zastosowano list comprehension, aby powtarzać przez wszystkie podzbiory w subsets_without_element 
# i dodać do nich element. 
# Na koniec zwracana jest lista wszystkich podzbiorów.
# https://docs.python.org/3/tutorial/datastructures.html 

x = ['a', 'b', 'c', 'd']
print(podzbiory(x[:]))    
