# -*- coding: utf-8 -*-
"""
@author: Karolina Majka
"""
import math  
# używamy funkcji matematycznych aby móc wprowadzić sqrt
# do obliczania pierwiastka kwadratowego.

def heron(a, b, c):
    # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf  
    # slajd 36 - Definiujemy funkcję heron, która przyjmuje trzy argumenty: 
    # długości boków trójkąta a, b i c.
    
    if a <= 0 or b <= 0 or c <= 0:
        # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 
        # slajdy 42-45 
        # Sprawdzenie, czy wszystkie boki są liczbami dodatnimi 
        return "Wszystkie boki muszą być większe od zera."
       # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf  
       # slajd 36 
       # Jeśli którykolwiek bok jest mniejszy lub równy zeru, funkcja 
       # zwraca komunikat o błędzie.
    
    
    if a + b <= c or a + c <= b or b + c <= a:
        # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 
        # slajdy 42-45 
        # Sprawdzamy nierówności trójkąta. 
        # a+b>c
        # a+c>b
        # b+c>a
        return "Te boki nie mogą tworzyć trójkąta."
    # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf  
    # slajd 36 
    # Jeśli boki nie spełniają nierówności trójkąta, funkcja zwraca komunikat o błędzie
    
    
    s = (a + b + c) / 2
    # Aby obliczyć pole trójkąta przy użyciu wzoru Herona, musimy najpierw obliczyć półobwód 
    # s a następnie użyć go do obliczenia pola S. 
    
   
    pole = math.sqrt(s * (s - a) * (s - b) * (s - c))
    
    return pole
# Jeśli warunki są spełnione, obliczamy półobwód s i pole trójkąta 
#za pomocą wzoru Herona, a następnie zwracamy wynik.


wynik = heron(3, 4, 5)
print(wynik)
# Próbujemy obliczyć pole trójkąta o bokach 3, 4 i 5. 
# Bezpośrednio drukujemy wynik funkcji. Jeśli wynik to komunikat o błędzie, zostanie on wyświetlony. 
#Jeśli wynik to liczba, zostanie wyświetlona wartość pola trójkąta.


