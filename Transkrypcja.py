# -*- coding: utf-8 -*-
"""
@author: Karolina Majka
"""

def komplement(nic_kodujaca):
    # Definiuje funkcję komplement, która przyjmuje jednen argument nic_kodujaca
    # będący sekwencją nici kodującej DNA.
   
    komplement = {'A': 'T', 'T': 'A', 'C': 'G', 'G': 'C'}
    # Tworzy listę zasad azotowych A, T, C i G
    # ich wartościami są ich komplementarnymi zasadami
    # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 55
    nic_matrycowa = ''.join(komplement[zasada] for zasada in nic_kodujaca)
    # Tworzy zmienną nic_matrycowa, która będzie przechowywać sekwencję nici matrycowej.
    # Powtarza po każdej zasadzie (A, T, C, G) w sekwencji nic_kodujaca.
    # Dla każdej zasady zasada pobiera jej komplementarną zasadę zdefiniowaną w liscie komplement.
    # Łączy komplementarne zasady w jedną ciągłą sekwencję.
    # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 55
    # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 47
    # https://eportal.pwr.edu.pl/pluginfile.php/395220/mod_resource/content/1/TP_wyklad.pdf 11
    # https://docs.python.org/3/library/os.path.html#os.path.join 
    
    return nic_matrycowa[::-1]
# Zwraca odwróconą sekwencję nic_matrycowa 
# nić matrycowa jest antyrównoległa do nici kodującej. 
# https://eportal.pwr.edu.pl/pluginfile.php/395220/mod_resource/content/1/TP_wyklad.pdf 31
# https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 57

def transkrybuj(nic_matrycowa):
   
    transkrypcja = {'A': 'U', 'T': 'A', 'C': 'G', 'G': 'C'}
    rna = ''.join(transkrypcja[zasada] for zasada in nic_matrycowa)
    return rna

# Funkcja tworzy sekwencję RNA na podstawie sekwencji nici matrycowej.
# Dokumentacja ta sama co wyżej


nic_kodujaca = "ATGC"
nic_matrycowa = komplement(nic_kodujaca)
print(f"Nici matrycowa dla nici kodującej {nic_kodujaca}: {nic_matrycowa}")

rna = transkrybuj(nic_matrycowa)
print(f"Sekwencja RNA dla nici matrycowej {nic_matrycowa}: {rna}")
