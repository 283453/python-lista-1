# -*- coding: utf-8 -*-
"""
@author: Karolina Majka
"""

def collatz(c0):
    # Definicja funkcji collatz, która przyjmuje jeden argument c0. 
    # Ten argument reprezentuje początkową wartość ciągu Collatza.
    sequence = [c0]
    # Tworzymy listę, która początkowo zawiera jedynie c0.
    # Będziemy dodawać do tej listy kolejne elementy ciągu.
    # https://eportal.pwr.edu.pl/pluginfile.php/395220/mod_resource/content/1/TP_wyklad.pdf 18, 19
    while c0 != 1:
        # Rozpoczynamy pętlę while, która będzie trwała, dopóki c0 nie osiągnie wartości 1. 
        # Warunek c0 != 1 sprawdza, czy c0 jest różne od 1.
        # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 34
        # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 46
        if c0 % 2 == 0:
            # Sprawdzamy, czy c0 jest parzyste. 
            # Operacja c0 % 2 daje resztę z dzielenia c0 przez 2. 
            # Jeśli reszta wynosi 0, oznacza to, że liczba jest parzysta.
            # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 33
            # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 42
            c0 = c0 // 2
            # Jeśli c0 jest parzyste, dzielimy je przez 2 
            # przypisujemy wynik z powrotem do c0. 
            # Operator // oznacza dzielenie całkowitoliczbowe (bez reszty).
            # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 32
            
        else:
            c0 = 3 * c0 + 1
            # else uruchamia się, jeśli c0 nie jest parzyste 
            # przekształcamy je według reguły Collatza: 3 * c0 + 1.
            # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 32
            # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 41
        sequence.append(c0)
        # Dodajemy nową wartość c0 do listy sequence.
        # https://eportal.pwr.edu.pl/pluginfile.php/395220/mod_resource/content/1/TP_wyklad.pdf 18, 19
    return sequence
# Po zakończeniu pętli (czyli gdy c0 stanie się 1), 
# zwracamy pełną listę sequence, która zawiera wszystkie elementy ciągu 
# od początkowej wartości c0 do 1.

def test_collatz(max_c0):
    # Definicja funkcji test_collatz, która przyjmuje jeden argument max_c0. 
    # Argument ten określa maksymalną wartość początkową c0 do testowania.
    # https://eportal.pwr.edu.pl/pluginfile.php/395220/mod_resource/content/1/TP_wyklad.pdf 25, 27, 
    max_value = 0
    max_length = 0
    max_value_c0 = 0
    max_length_c0 = 0
    # Tworzymy cztery zmienne:
    # max_value przechowuje maksymalną wartość elementu ciągu znalezioną do tej pory.
    # max_length przechowuje maksymalną długość ciągu.
    # max_value_c0 przechowuje wartość c0, dla której znaleziono maksymalną wartość elementu ciągu.
    # max_length_c0 przechowuje wartość c0, dla której znaleziono maksymalną długość ciągu.
    # https://docs.python.org/3/library/functions.html#max
    for c0 in range(1, max_c0 + 1):
        # Pętla for powtarza przez wszystkie wartości c0 od 1 do max_c0 włącznie.
        # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 47
        seq = collatz(c0)
        # Dla każdej wartości c0 wywołujemy funkcję collatz, 
        # aby wygenerować odpowiedni ciąg, i przypisujemy wynik do seq.
        # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 55
        current_max_value = max(seq)
        # Znajdujemy maksymalną wartość w ciągu seq za pomocą funkcji max 
        # przypisujemy ją do current_max_value
        # https://docs.python.org/3/library/functions.html
        current_length = len(seq)
        # Obliczamy długość ciągu seq za pomocą funkcji len i przypisujemy ją do current_length.
        # https://docs.python.org/3/library/functions.html 
        # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 66
    
        
        if current_max_value > max_value:
            # Sprawdzamy, czy maksymalna wartość w obecnym ciągu (current_max_value)
            # jest większa niż dotychczas znaleziona max_value.
            max_value = current_max_value
            max_value_c0 = c0
            # Jeśli current_max_value jest większa, aktualizujemy max_value 
            # zapisujemy bieżącą wartość c0 do max_value_c0.
        
        if current_length > max_length:
            # Sprawdzamy, czy długość obecnego ciągu (current_length) jest większa 
            # niż dotychczas znaleziona max_length
            max_length = current_length
            max_length_c0 = c0
            # Jeśli current_length jest większa, aktualizujemy max_length 
            # zapisujemy bieżącą wartość c0 do max_length_c0.
            
            # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 42
    
    print(f"Maksymalna wartość elementu ciągu: {max_value}, dla c0: {max_value_c0}")
    print(f"Maksymalna długość ciągu: {max_length}, dla c0: {max_length_c0}")
# Po zakończeniu pętli drukujemy maksymalną wartość elementu ciągu i odpowiadającą jej wartość c0,
# a także maksymalną długość ciągu i odpowiadającą jej wartość c0.
max_c0 = 1000  
test_collatz(max_c0)
# Ustawiamy max_c0 na 1000
# wywołujemy funkcję test_collatz, aby przetestować wartości c0 od 1 do 1000. 
