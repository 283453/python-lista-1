# -*- coding: utf-8 -*-
"""
@author: Karolina Majka
"""
def fibonacci(n):
    # Tworzymy funkcję o nazwie fibonacci, która przyjmuje jeden argument n.
    # Argument n reprezentuje liczbę pierwszych elementów ciągu Fibonacciego, które chcemy uzyskać
    # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf
    if not isinstance(n, int) or n <= 0:
        # Sprawdzamy, czy n jest liczbą całkowitą (int).
        # Jeśli nie, warunek not isinstance(n, int) zwróci True
        # Sprawdzamy, czy n jest liczbą większą od zera. 
        # Jeśli n jest mniejsze lub równe zero, warunek n <= 0 zwróci True.
        # https://eportal.pwr.edu.pl/pluginfile.php/395220/mod_resource/content/1/TP_wyklad.pdf 8
        # https://stackoverflow.com/questions/11748671/negative-form-of-isinstance-in-python
        raise ValueError("n musi być liczbą całkowitą większą od zera")
        # Jeśli którykolwiek z tych warunków jest spełniony 
        # czyli n nie jest liczbą całkowitą lub jest mniejsze/równe zero,
        # podnosimy wyjątek ValueError z odpowiednią wiadomością.
        # https://eportal.pwr.edu.pl/pluginfile.php/395220/mod_resource/content/1/TP_wyklad.pdf 8 i 9
    
    fib_sequence = [0, 1]
    # Tworzymy listę fib_sequence, która zawiera pierwsze dwa elementy 
    # ciągu Fibonacciego, czyli 0 i 1.
    # https://eportal.pwr.edu.pl/pluginfile.php/395220/mod_resource/content/1/TP_wyklad.pdf 18
    while len(fib_sequence) < n:
        # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 66
        # Pętla while działa tak długo, jak długo długość listy fib_sequence jest mniejsza od n.
        # Oznacza to, że będziemy dodawać kolejne elementy do listy, aż osiągniemy pożądaną liczbę elementów.
        fib_sequence.append(fib_sequence[-1] + fib_sequence[-2])
        # https://eportal.pwr.edu.pl/pluginfile.php/395220/mod_resource/content/1/TP_wyklad.pdf 18
        # https://eportal.pwr.edu.pl/pluginfile.php/395220/mod_resource/content/1/TP_wyklad.pdf 15, 68, 70, 77
        # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 59, 97
        # Dodajemy nowy element do listy fib_sequence, który jest sumą dwóch ostatnich elementów 
        # tej listy (fib_sequence[-1] i fib_sequence[-2]). 
        # W Pythonie indeks -1 odnosi się do ostatniego elementu listy, a -2 do przedostatniego elementu.
        # https://www.ekoportal.gov.pl/fileadmin/user_upload/v2_Python.pdf 57
    return fib_sequence[:n]
        # Zwracamy pierwsze n elementów listy fib_sequence. 
        # [:n] oznacza, że bierzemy elementy od początku listy do indeksu n (nie włącznie z n).
        # https://stackoverflow.com/questions/17958667/in-python-what-does-n-do 
print(fibonacci(10))  
# Wywołujemy pierwsze 10 elementów ciągu Fibonacciego.
